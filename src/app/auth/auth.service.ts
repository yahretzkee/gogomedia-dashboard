import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http"
import { environment } from "src/environments/environment";
import { map } from "rxjs/operators";
import { User } from "../store/user/user";
import { Observable } from "rxjs";
import jwtDecode from "jwt-decode";

interface JWTPayload {
    role: string;
}

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    constructor(private http: HttpClient) {

    }

    getToken() {
        return localStorage.getItem('token');
    }

    isAuthenticated(): boolean {
        const token = this.getToken();
        if (token) {
            const payload = <JWTPayload>jwtDecode(token);
            if (payload.role === 'ADMIN') {
                return true;
            }
        }
        return false;
    }

    signIn(username: string, password: string): Observable<User> {
        return this.http.post<User>(environment.baseUrl + '/users/signin', { username, password })
    }

}