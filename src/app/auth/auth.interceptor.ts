import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from "@angular/common/http";
import { Injectable, Injector } from "@angular/core";
import { Observable, of } from "rxjs";
import { AuthService } from "./auth.service";

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
    constructor(private injector: Injector, private authService: AuthService) {
    }

    intercept(request: HttpRequest<Object>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (request.method === "POST" && request.url === "http://localhost:4200/users/signin") {
            return next.handle(request);
        }
        if (this.authService.isAuthenticated()) {
            const token = this.authService.getToken();
            if (token) {
                request = this.addToken(request, token);
            }
        }
        return next.handle(request);
    }

    private addToken(request: HttpRequest<any>, token: string) {
        return request.clone({
            setHeaders: {
                'Authorization': `Bearer ${token}`
            }
        });
    }
}