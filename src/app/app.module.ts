import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthGuard } from './auth/auth.guard';
import { AuthInterceptor } from './auth/auth.interceptor';
import { BackendInterceptor } from './backend/backend.interceptor';
import { DashboardComponent } from './dashboard/dashboard.component';
import { UserService } from './services-http/user.service';
import { SigninPageComponent } from './signin-page/signin-page.component';
import { UsersEffects } from './store/user/users.effects';
import * as fromUser from './store/user/users.reducers';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    SigninPageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    StoreModule.forRoot({ users: fromUser.reducer }),
    EffectsModule.forRoot([UsersEffects]),
    StoreDevtoolsModule.instrument(),
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: BackendInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },
    AuthGuard,
    UserService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
