import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from "@angular/common/http";
import { Injectable, Injector } from "@angular/core";
import { Observable, of } from "rxjs";

const userData = {
    "id": "1",
    "name": "Piotr",
    "surname": "Chabros",
    "email": "piotrchabros@outlook.com",
    "role": "ADMIN",
    "authToken": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlIjoiQURNSU4ifQ.29eLEIGd2z128wIMmHaMfmN-OeZn2YYJEE6fuxgjmBI"
}

@Injectable()
export class BackendInterceptor implements HttpInterceptor {
    constructor(private injector: Injector) {
    }

    intercept(request: HttpRequest<Object>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (request.method === "POST" && request.url === "http://localhost:4200/users/signin") {
            return of(new HttpResponse({ status: 200, body: userData }));
        }
        return next.handle(request);
    }
}