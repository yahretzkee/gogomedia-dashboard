import { props } from "@ngrx/store";
import { createAction } from "@ngrx/store";
import { User } from "./user";

export const signIn = createAction(
    '[Users] Sign In',
    props<{ username: string, password: string }>()
)

export const signInSuccess = createAction(
    '[Users] Sign In Success',
    props<{ user: User }>()
)

export const signInError = createAction(
    '[Users] Sign In Error',
    props<{ error: any }>()
);

export const signOut = createAction(
    '[Users] Sign Out'
)

export const signOutSuccess = createAction(
    '[Users] Sign Out Success'
)