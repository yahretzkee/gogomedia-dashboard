import { createEntityAdapter, EntityAdapter, EntityState } from "@ngrx/entity";
import { Action, createReducer, on } from "@ngrx/store";
import { User } from "./user";
import { signIn, signInSuccess, signOutSuccess } from "./users.actions";

export interface UsersState extends EntityState<User> {
    isUserSignedIn: boolean;
    signedInUserId: string | null;
}

const adapter: EntityAdapter<User> = createEntityAdapter<User>({
    selectId: (user) => user.id,
});

const initialState: UsersState = adapter.getInitialState({
    isUserSignedIn: false,
    signedInUserId: null,
});

export const getSignedInUserId = (state: UsersState) => state.signedInUserId;

export const usersReducer = createReducer(
    initialState,
    on(signInSuccess, (state, { user }) => {
        return adapter.upsertOne(user, {
            ...state,
            isUserSignedIn: true
        })
    }),
    on(signOutSuccess, (state) => {
        return adapter.removeAll({
            ...state,
            isUserSignedIn: false
        });
    })
)

export function reducer(state: UsersState | undefined, action: Action) {
    return usersReducer(state, action);
}

const getSelectedUserId = (state: UsersState) => state.signedInUserId;

// get the selectors
const {
    selectIds,
    selectEntities,
    selectAll,
    selectTotal,
} = adapter.getSelectors();

// select the array of user ids
export const selectUserIds = selectIds;

// select the dictionary of user entities
export const selectUserEntities = selectEntities;

// select the array of users
export const selectAllUsers = selectAll;

// select the total user count
export const selectUserTotal = selectTotal;