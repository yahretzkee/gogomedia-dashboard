import { Actions, createEffect, ofType } from "@ngrx/effects";
import { AuthService } from "src/app/auth/auth.service";
import { signIn, signInSuccess, signOut, signOutSuccess } from "./users.actions";
import { map, mergeMap, catchError, switchMap, tap } from 'rxjs/operators';
import { Injectable } from "@angular/core";
import { Router } from "@angular/router";

@Injectable()
export class UsersEffects {

    signIn$ = createEffect(() =>
        this.actions$.pipe(
            ofType(signIn),
            mergeMap(({ username, password }) =>
                this.authService.signIn(username, password)
                    .pipe(
                        map(user => {
                            return signInSuccess({ user });
                        })
                    )
            )
        )
    );

    signInSuccess = createEffect(() =>
        this.actions$.pipe(
            ofType(signInSuccess),
            tap(({ user }) => localStorage.setItem('token', user.authToken))
        )
        , { dispatch: false });

    signOut$ = createEffect(() =>
        this.actions$.pipe(
            ofType(signOut),
            map(() => {
                localStorage.removeItem('token');
                this.router.navigate(['signin']);
                return signOutSuccess();
            })
        )
    )

    constructor(private actions$: Actions,
        private router: Router,
        private authService: AuthService) { }
}
