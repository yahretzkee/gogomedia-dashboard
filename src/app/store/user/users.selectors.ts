import { ActionReducerMap, createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromUser from './users.reducers';

export interface UserState {
    users: fromUser.UsersState;
}

export const selectUserState = createFeatureSelector<fromUser.UsersState>('users');

export const selectUserIds = createSelector(
    selectUserState,
    fromUser.selectUserIds // shorthand for usersState => fromUser.selectUserIds(usersState)
);
export const selectUserEntities = createSelector(
    selectUserState,
    fromUser.selectUserEntities
);
export const selectAllUsers = createSelector(
    selectUserState,
    fromUser.selectAllUsers
);
export const selectUserTotal = createSelector(
    selectUserState,
    fromUser.selectUserTotal
);
export const selectCurrentUserId = createSelector(
    selectUserState,
    fromUser.getSignedInUserId
);

export const selectCurrentUser = createSelector(
    selectUserEntities,
    selectCurrentUserId,
    (userEntities, userId) => userId ? userEntities[userId] : null
);

export const isUserSignedIn = createSelector(
    selectUserState,
    (state) => state.isUserSignedIn
);