export enum USER_ROLE {
    ADMIN, USER
}

export interface User {
    id: string;
    name: string;
    surname: string;
    email: string;
    role: USER_ROLE;
    authToken: string;
}