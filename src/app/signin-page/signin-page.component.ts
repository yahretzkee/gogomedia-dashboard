import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { filter, tap } from 'rxjs/operators';
import { signIn } from '../store/user/users.actions';
import { isUserSignedIn } from '../store/user/users.selectors';

@Component({
  selector: 'app-signin-page',
  templateUrl: './signin-page.component.html',
  styleUrls: ['./signin-page.component.scss']
})
export class SigninPageComponent implements OnInit, OnDestroy {

  userForm = new FormGroup({
    username: new FormControl('', [Validators.required]),
    password: new FormControl('', [Validators.required])
  })

  private subscription = new Subscription();

  constructor(private store: Store, private router: Router) { }

  ngOnInit(): void {
    this.subscription.add(this.store.select(isUserSignedIn)
      .pipe(
        filter((isUserSignedIn) => isUserSignedIn === true),
        tap(() => this.router.navigate(['dashboard']))
      ).subscribe());
  }

  signIn() {
    this.store.dispatch(signIn(this.userForm.getRawValue()))
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

}
