import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { UserService } from '../services-http/user.service';
import { signOut } from '../store/user/users.actions';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  constructor(private store: Store,
    private http: HttpClient,
    private userService: UserService) { }

  ngOnInit(): void {
  }

  sendAuthenticatedRequest() {
    this.userService.getUsers().subscribe();
  }

  signOut() {
    this.store.dispatch(signOut());
  }

}
